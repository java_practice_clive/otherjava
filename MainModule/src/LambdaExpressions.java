import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

class Product{
    int id;
    String name;
    float price;

    public Product(int id, String name, float price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}

public class LambdaExpressions {

    public static void main(String[] args) {
        List<Product> products = fillProducts();

        System.out.println("LIST BEFORE SORT =======");
        printProducts(products);

        // lambda expression with comparator
        Collections.sort(products, Comparator.comparing(p -> p.name));

        System.out.println("LIST AFTER LAMBDA SORT =======");
        printProducts(products);

        // Using stream to filter products.
        Stream<Product> filteredData = products.stream().filter(
                product -> product.price > 1000);
        filteredData.forEach(
                (product) -> System.out.println(product.name + " " + product.price)
        );

    }
    private static List<Product> fillProducts(){
        List<Product> products = new ArrayList<>();
        products.add(new Product(1, "MacBook Pro", 1200));
        products.add(new Product(2, "iPhone 7", 700));
        products.add(new Product(3, "Samsung Galaxy S9", 900));
        products.add(new Product(4, "LG Smart TV", 4000));
        return products;
    }
    private static void printProducts(List<Product> products){
        for(Product product: products){
            System.out.println(product.id + " " + product.name + " " + product.price);
        }
        System.out.println();
    }
}
