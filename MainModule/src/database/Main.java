package database;

import database.Helper.DBHandler;

import java.sql.*;

public class Main {
    static private DBHandler dbHandler;
    static private Connection connection;
    static private PreparedStatement preparedStatement;

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        dbHandler = new DBHandler();
        connection = dbHandler.getDbConnection();

        //writeToDB();
        //readFromDB();
        //updateDB("Soska", "Prisoska", "sopri",
        //        "Neverland", 17, 2);
        deleteFromDB(2);
    }

    private static void writeToDB() throws SQLException {
        String insert = "INSERT INTO users(" +
                "firstname,lastname,username,address,age)" +
                "VALUES(?,?,?,?,?)";
        preparedStatement = connection.prepareStatement(insert);
        preparedStatement.setString(1, "Taro");
        preparedStatement.setString(2, "Faro");
        preparedStatement.setString(3,"pupsik21");
        preparedStatement.setString(4, "LA");
        preparedStatement.setInt(5, 31);
        preparedStatement.executeUpdate();
        preparedStatement.close();
    }

    private static void readFromDB() throws SQLException{
        String query =" SELECT * from users";
        preparedStatement = connection.prepareStatement(query);

        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            System.out.println("Names: " + resultSet.getString("firstname")+
                    " / Last Name: " + resultSet.getString("lastname"));
        }
    }
    private static void updateDB(String firstName, String lastName,
                                 String username, String address,
                                 int age, int id) throws SQLException{
        String query = "UPDATE users SET firstname = ?, lastname = ?, username = ?, " +
                "address = ?, age = ? where userid = ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, firstName);
        preparedStatement.setString(2, lastName);
        preparedStatement.setString(3, username);
        preparedStatement.setString(4, address);
        preparedStatement.setInt(5, age);
        preparedStatement.setInt(6, id);
        preparedStatement.executeUpdate();
        preparedStatement.close();

    }

    public static void deleteFromDB(int id) throws SQLException {
        String query ="DELETE from users where userid = ?";
        preparedStatement = connection.prepareStatement(query);
        preparedStatement.setInt(1, id);
        preparedStatement.execute();
        preparedStatement.close();
    }
}
